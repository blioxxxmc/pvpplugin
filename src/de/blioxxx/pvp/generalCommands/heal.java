package de.blioxxx.pvp.generalCommands;

import de.blioxxx.pvp.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class heal  implements CommandExecutor{

    private Main Plugin;

    public heal(Main Plugin) {
        this.Plugin = Plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        Player p = (Player)sender;
        p.setHealth(20.0);
        p.sendMessage("Du wurdest auf 10 Herzen geheilt");

        return true;
    }
}
