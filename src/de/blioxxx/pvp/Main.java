package de.blioxxx.pvp;

import de.blioxxx.pvp.generalCommands.heal;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.Console;

public class Main extends JavaPlugin {

    @Override
    public void onEnable(){
        System.out.println(ChatColor.GREEN + "PvP Plugin by BlioxxxDev wurde gestartet!");

        registerCommands();
        initConfig();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        //Infos
        //Command execute as Player: Bukkit.dispatchCommand(p, "info");
        //Command execute as Console: Bukkit.getServer().dispatchCommand(getServer().getConsoleSender(), "give "+p.getName()+" iron_sword 1");

        //Umwandlung Sender->Player
        Player p = null;

        if (sender instanceof Player){
            p = (Player)sender;
        }

        String Version = this.getDescription().getVersion();

        //Info Kommando
        if(cmd.getName().equalsIgnoreCase("info")) {
            if (p != null) {
                p.sendMessage(ChatColor.GREEN + "PvP Plugin wurde geladen.");
                p.sendMessage(ChatColor.GREEN + Version + " by "+this.getConfig().getString("PVPPlugin.commands.info.message.author"));
                return true;
            }
        }

        if (cmd.getName().equalsIgnoreCase("kit")){
            if(p != null){
                Bukkit.getServer().dispatchCommand(getServer().getConsoleSender(), "give "+p.getName()+" iron_sword 1");
                Bukkit.getServer().dispatchCommand(getServer().getConsoleSender(), "give "+p.getName()+" iron_helmet 1");
                Bukkit.getServer().dispatchCommand(getServer().getConsoleSender(), "give "+p.getName()+" iron_chestplate 1");
                Bukkit.getServer().dispatchCommand(getServer().getConsoleSender(), "give " + p.getName() + " iron_leggings 1");
                Bukkit.getServer().dispatchCommand(getServer().getConsoleSender(), "give " + p.getName() + " iron_boots 1");

            return true;
            }
        }

        if (cmd.getName().equalsIgnoreCase("pvp")) {
            if (args.length > 0){
                if (p != null) {
                    if (args[0] == "start"){
                        int numberOfArgs = args.length;
                        numberOfArgs = numberOfArgs - 1;

                        //For Player
                        for (int i = 0; i < numberOfArgs; i++){
                            p.sendMessage(i+" Spieler");
                        }
                        return true;
                    }
                }
            }
        }

        return false;
    }


    public void registerCommands(){
        heal healCommand = new heal(this);
        getCommand("heal").setExecutor(healCommand);
    }

    public void initConfig(){
        this.reloadConfig();

        this.getConfig().options().header("Willkommen in der Config Datei");
        this.getConfig().addDefault("PVPPlugin.commands.info.message.author","BlioxxxDev");

        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }


}